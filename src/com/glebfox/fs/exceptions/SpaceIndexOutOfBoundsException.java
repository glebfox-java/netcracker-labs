package com.glebfox.fs.exceptions;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public class SpaceIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private static final long serialVersionUID = -8872777760365116415L;

    public SpaceIndexOutOfBoundsException() {
    }

    public SpaceIndexOutOfBoundsException(String message) {
        super(message);
    }
}
