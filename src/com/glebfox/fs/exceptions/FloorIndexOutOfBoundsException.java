package com.glebfox.fs.exceptions;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public class FloorIndexOutOfBoundsException extends IndexOutOfBoundsException {
    private static final long serialVersionUID = 1061348949279268680L;

    public FloorIndexOutOfBoundsException() {
    }

    public FloorIndexOutOfBoundsException(String message) {
        super(message);
    }
}
