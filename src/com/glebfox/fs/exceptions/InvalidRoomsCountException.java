package com.glebfox.fs.exceptions;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public class InvalidRoomsCountException extends IllegalArgumentException {
    private static final long serialVersionUID = -7184241916416017748L;

    public InvalidRoomsCountException() {
    }

    public InvalidRoomsCountException(String message) {
        super(message);
    }

    public InvalidRoomsCountException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRoomsCountException(Throwable cause) {
        super(cause);
    }
}
