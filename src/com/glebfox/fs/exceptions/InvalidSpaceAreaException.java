package com.glebfox.fs.exceptions;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public class InvalidSpaceAreaException extends IllegalArgumentException {
    private static final long serialVersionUID = 5510143542444092429L;

    public InvalidSpaceAreaException() {
    }

    public InvalidSpaceAreaException(String message) {
        super(message);
    }

    public InvalidSpaceAreaException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSpaceAreaException(Throwable cause) {
        super(cause);
    }
}
