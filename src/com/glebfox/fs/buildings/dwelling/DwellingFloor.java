package com.glebfox.fs.buildings.dwelling;

import com.glebfox.fs.buildings.AbstractFloor;
import com.glebfox.fs.buildings.Space;

import java.util.ArrayList;

/**
 * @author glebfox
 */
public class DwellingFloor extends AbstractFloor {

    private static final long serialVersionUID = 1357066263071625248L;

    public DwellingFloor(int flatsCount) {
        spaces = new ArrayList<>(flatsCount);
        for (int i = 0; i < flatsCount; i++) {
            spaces.add(new Flat());
        }
    }

    public DwellingFloor(Space... spaces) {
        super(spaces);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && (obj instanceof DwellingFloor);
    }

    @Override
    public DwellingFloor clone() throws CloneNotSupportedException {
        return (DwellingFloor) super.clone();
    }
}
