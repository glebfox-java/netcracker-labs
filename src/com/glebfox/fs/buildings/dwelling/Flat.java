package com.glebfox.fs.buildings.dwelling;

import com.glebfox.fs.buildings.AbstractSpace;

/**
 * @author glebfox
 */
public class Flat extends AbstractSpace {

    private static final long serialVersionUID = 7233133606987778662L;

    public Flat() {
        super();
    }

    public Flat(double area) {
        super(area);
    }

    public Flat(double area, int roomsCount) {
        super(area, roomsCount);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && (obj instanceof Flat);
    }

    @Override
    public Flat clone() throws CloneNotSupportedException {
        return (Flat) super.clone();
    }
}
