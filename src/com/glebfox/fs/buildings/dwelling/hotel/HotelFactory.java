package com.glebfox.fs.buildings.dwelling.hotel;

import com.glebfox.fs.buildings.Floor;
import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.Building;
import com.glebfox.fs.buildings.BuildingFactory;
import com.glebfox.fs.buildings.dwelling.Flat;

/**
 * @author glebfox
 */
public class HotelFactory implements BuildingFactory {
    @Override
    public Space createSpace(double area) {
        return new Flat(area);
    }

    @Override
    public Space createSpace(double area, int roomsCount) {
        return new Flat(area, roomsCount);
    }

    @Override
    public Floor createFloor(int spacesCount) {
        return new HotelFloor(spacesCount);
    }

    @Override
    public Floor createFloor(Space... spaces) {
        return new HotelFloor(spaces);
    }

    @Override
    public Building createBuilding(int... spacesCounts) {
        return new Hotel(spacesCounts);
    }

    @Override
    public Building createBuilding(Floor... floors) {
        return new Hotel(floors);
    }
}
