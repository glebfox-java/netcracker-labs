package com.glebfox.fs.buildings.dwelling.hotel;

import com.glebfox.fs.buildings.Floor;
import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.dwelling.Dwelling;

/**
 * @author glebfox
 */
public class Hotel extends Dwelling {
    private static final long serialVersionUID = 6462503836053934555L;

    public Hotel(int... spacesCounts) {
        super(spacesCounts.length);
        for (int spacesCount : spacesCounts) {
            floors.add(new HotelFloor(spacesCount));
        }
    }

    public Hotel(Floor... floors) {
        super(floors);
    }

    public int getLuxury() {
        int luxury = 0;
        Floor[] floors = getFloors();
        for (Floor floor : floors) {
            if (floor instanceof HotelFloor) {
                int current = ((HotelFloor) floor).getStars();
                if (current > luxury)
                    luxury = current;
            }
        }
        return luxury;
    }

    @Override
    public Space getBestSpace() {
        Space bestSpace = null;
        double bestValue = 0;
        Floor[] floors = getFloors();
        for (Floor floor : floors) {
            if (floor instanceof HotelFloor) {
                double currentCoeff = getCoeff(((HotelFloor) floor).getStars());
                Space[] spaces = floor.getSpaces();
                for (Space space : spaces) {
                    double currentValue = space.getArea() * currentCoeff;
                    if (currentValue > bestValue) {
                        bestSpace = space;
                        bestValue = currentValue;
                    }
                }
            }
        }

        return bestSpace;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && (obj instanceof Hotel);
    }

    @Override
    public Hotel clone() throws CloneNotSupportedException {
        return (Hotel) super.clone();
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder(getClass().getSimpleName());
        info.append(" (").append(getLuxury());
        info.append(", ").append(getFloorsCount());
        for (Floor floor : floors) {
            info.append(", ").append(floor.toString());
        }

        return info.append(")").toString();
    }

    private double getCoeff(int stars) {
        switch (stars) {
            case 1:
                return 0.25;
            case 2:
                return 0.5;
            case 3:
                return 1;
            case 4:
                return 1.25;
            case 5:
                return 1.5;
        }
        return 0;
    }
}
