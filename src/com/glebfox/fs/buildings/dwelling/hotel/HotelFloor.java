package com.glebfox.fs.buildings.dwelling.hotel;

import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.dwelling.DwellingFloor;

/**
 * @author glebfox
 */
public class HotelFloor extends DwellingFloor {

    private static final long serialVersionUID = -5620628387014310870L;

    private final int DEFAULT_STARS = 1;

    private int stars = DEFAULT_STARS;

    public HotelFloor(int flatsCount) {
        super(flatsCount);
    }

    public HotelFloor(Space... spaces) {
        super(spaces);
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HotelFloor)) return false;
        if (!super.equals(o)) return false;

        HotelFloor that = (HotelFloor) o;

        return stars == that.stars;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + stars;
        return result;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder(getClass().getSimpleName());
        info.append(" (").append(getStars());
        info.append(", ").append(getSpacesCount());
        for (Space space : spaces) {
            info.append(", ").append(space.toString());
        }
        return info.append(")").toString();
    }

    @Override
    public HotelFloor clone() throws CloneNotSupportedException {
        return (HotelFloor) super.clone();
    }
}
