package com.glebfox.fs.buildings.dwelling;


import com.glebfox.fs.buildings.AbstractBuilding;
import com.glebfox.fs.buildings.Floor;

/**
 * @author glebfox
 */
public class Dwelling extends AbstractBuilding {
    private static final long serialVersionUID = -5392174930275157364L;

    protected Dwelling(int size) {
        super(size);
    }

    public Dwelling(int... spacesCounts) {
        super(spacesCounts.length);

        for (int spacesCount : spacesCounts) {
            floors.add(new DwellingFloor(spacesCount));
        }
    }

    public Dwelling(Floor... floors) {
        super(floors);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof Dwelling);
    }

    @Override
    public Dwelling clone() throws CloneNotSupportedException {
        return (Dwelling) super.clone();
    }
}
