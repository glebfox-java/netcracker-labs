package com.glebfox.fs.buildings.dwelling;

import com.glebfox.fs.buildings.Building;
import com.glebfox.fs.buildings.BuildingFactory;
import com.glebfox.fs.buildings.Floor;
import com.glebfox.fs.buildings.Space;

/**
 * @author glebfox
 */
public class DwellingFactory implements BuildingFactory {
    @Override
    public Space createSpace(double area) {
        return new Flat(area);
    }

    @Override
    public Space createSpace(double area, int roomsCount) {
        return new Flat(area, roomsCount);
    }

    @Override
    public Floor createFloor(int spacesCount) {
        return new DwellingFloor(spacesCount);
    }

    @Override
    public Floor createFloor(Space... spaces) {
        return new DwellingFloor(spaces);
    }

    @Override
    public Building createBuilding(int... spacesCounts) {
        return new Dwelling(spacesCounts);
    }

    @Override
    public Building createBuilding(Floor... floors) {
        return new Dwelling(floors);
    }
}
