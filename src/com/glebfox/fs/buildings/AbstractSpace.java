package com.glebfox.fs.buildings;

import com.glebfox.fs.exceptions.InvalidRoomsCountException;
import com.glebfox.fs.exceptions.InvalidSpaceAreaException;

/**
 * @author glebfox
 */
public abstract class AbstractSpace implements Space {

    private static final long serialVersionUID = -9047053661776609992L;

    protected static final double DEFAULT_AREA = 50.0;
    protected static final int DEFAULT_ROOMS_COUNT = 2;

    protected double area;
    protected int roomsCount;

    protected AbstractSpace() {
        this(DEFAULT_AREA, DEFAULT_ROOMS_COUNT);
    }

    protected AbstractSpace(double area) {
        this(area, DEFAULT_ROOMS_COUNT);
    }

    protected AbstractSpace(double area, int roomsCount) {
        setArea(area);
        setRoomsCount(roomsCount);
    }

    @Override
    public double getArea() {
        return area;
    }

    @Override
    public void setArea(double area) {
        if (area <= 0) {
            throw new InvalidSpaceAreaException();
        }
        this.area = area;
    }

    @Override
    public int getRoomsCount() {
        return roomsCount;
    }

    @Override
    public void setRoomsCount(int roomsCount) {
        if (roomsCount <= 0) {
            throw new InvalidRoomsCountException();
        }
        this.roomsCount = roomsCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractSpace)) return false;

        AbstractSpace that = (AbstractSpace) o;

        return Double.compare(that.area, area) == 0 && roomsCount == that.roomsCount;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(area);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + roomsCount;
        return result;
    }

    @Override
    public AbstractSpace clone() throws CloneNotSupportedException {
        return (AbstractSpace) super.clone();
    }

    @Override
    public String toString() {
        return String.format("%s (%d %.1f)", getClass().getSimpleName(), roomsCount, area);
    }
}
