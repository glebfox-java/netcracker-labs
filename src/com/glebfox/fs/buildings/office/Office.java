package com.glebfox.fs.buildings.office;

import com.glebfox.fs.buildings.AbstractSpace;

/**
 * @author glebfox
 */
public class Office extends AbstractSpace {

    private static final long serialVersionUID = -248616190901539555L;

    public Office() {
        super();
    }

    public Office(double area) {
        super(area);
    }

    public Office(double area, int roomsCount) {
        super(area, roomsCount);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && (obj instanceof Office);
    }

    @Override
    public Office clone() throws CloneNotSupportedException {
        return (Office) super.clone();
    }
}
