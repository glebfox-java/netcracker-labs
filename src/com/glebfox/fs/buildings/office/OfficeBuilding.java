package com.glebfox.fs.buildings.office;

import com.glebfox.fs.buildings.AbstractBuilding;
import com.glebfox.fs.buildings.Floor;

/**
 * @author glebfox
 */
public class OfficeBuilding extends AbstractBuilding {

    private static final long serialVersionUID = -6009054945686410680L;

    public OfficeBuilding(int... spacesCounts) {
        super(spacesCounts.length);

        for (int spacesCount : spacesCounts) {
            floors.add(new OfficeFloor(spacesCount));
        }
    }

    public OfficeBuilding(Floor... floors) {
        super(floors);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) && (o instanceof OfficeBuilding);
    }

    @Override
    public OfficeBuilding clone() throws CloneNotSupportedException {
        return (OfficeBuilding) super.clone();
    }
}
