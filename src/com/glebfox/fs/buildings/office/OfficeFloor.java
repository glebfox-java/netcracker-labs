package com.glebfox.fs.buildings.office;

import com.glebfox.fs.buildings.AbstractFloor;
import com.glebfox.fs.buildings.Space;

import java.util.ArrayList;

/**
 * @author glebfox
 */
public class OfficeFloor extends AbstractFloor {

    private static final long serialVersionUID = 8159142816720568357L;

    public OfficeFloor(int officesCount) {
        spaces = new ArrayList<>(officesCount);
        for (int i = 0; i < officesCount; i++) {
            spaces.add(new Office());
        }
    }

    public OfficeFloor(Space... spaces) {
        super(spaces);
    }

    @Override
    public OfficeFloor clone() throws CloneNotSupportedException {
        return (OfficeFloor) super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && (obj instanceof OfficeFloor);
    }
}
