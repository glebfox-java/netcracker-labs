package com.glebfox.fs.buildings.office;

import com.glebfox.fs.buildings.Floor;
import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.Building;
import com.glebfox.fs.buildings.BuildingFactory;

/**
 * @author glebfox
 */
public class OfficeFactory implements BuildingFactory {
    @Override
    public Space createSpace(double area) {
        return new Office(area);
    }

    @Override
    public Space createSpace(double area, int roomsCount) {
        return new Office(area, roomsCount);
    }

    @Override
    public Floor createFloor(int spacesCount) {
        return new OfficeFloor(spacesCount);
    }

    @Override
    public Floor createFloor(Space... spaces) {
        return new OfficeFloor(spaces);
    }

    @Override
    public Building createBuilding(int... spacesCounts) {
        return new OfficeBuilding(spacesCounts);
    }

    @Override
    public Building createBuilding(Floor... floors) {
        return new OfficeBuilding(floors);
    }
}
