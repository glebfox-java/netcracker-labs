package com.glebfox.fs.buildings;

import java.io.*;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public final class Utils {

    private Utils() {
        throw new UnsupportedOperationException();
    }

    public static <T> T getDeepCloning(T obj) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            // noinspection unchecked
            return (T) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
