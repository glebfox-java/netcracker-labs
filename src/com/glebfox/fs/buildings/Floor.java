package com.glebfox.fs.buildings;

import java.io.Serializable;

/**
 * @author glebfox
 */
public interface Floor extends Iterable<Space>, Serializable, Cloneable {

    int getSpacesCount();

    double getTotalArea();

    int getTotalRoomsCount();

    Space[] getSpaces();

    Space getSpace(int spaceNumber);

    void setSpace(int spaceNumber, Space newSpace);

    void addSpace(int spaceNumber, Space newSpace);

    void removeSpace(int spaceNumber);

    Space getBestSpace();

    Floor clone() throws CloneNotSupportedException;
}
