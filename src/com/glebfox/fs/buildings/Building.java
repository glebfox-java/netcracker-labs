package com.glebfox.fs.buildings;

import java.io.Serializable;

/**
 * @author glebfox
 */
public interface Building extends Iterable<Floor>, Serializable, Cloneable {

    int getFloorsCount();

    int getSpacesCount();

    double getTotalArea();

    int getTotalRoomsCount();

    Floor[] getFloors();

    Floor getFloor(int floorNumber);

    void setFloor(int floorNumber, Floor newFloor);

    Space getSpace(int spaceNumber);

    void setSpace(int spaceNumber, Space newSpace);

    void addSpace(int spaceNumber, Space newSpace);

    void removeSpace(int spaceNumber);

    Space getBestSpace();

    Space[] getSortedSpaces();

    Building clone() throws CloneNotSupportedException;
}
