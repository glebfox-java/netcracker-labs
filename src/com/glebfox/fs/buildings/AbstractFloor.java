package com.glebfox.fs.buildings;

import com.glebfox.fs.exceptions.SpaceIndexOutOfBoundsException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author glebfox
 */
public abstract class AbstractFloor implements Floor {

    private static final long serialVersionUID = -3210674776174131078L;

    protected List<Space> spaces;

    protected AbstractFloor(Space... spaces) {
        this.spaces = new ArrayList<>(spaces.length);
        Collections.addAll(this.spaces, spaces);
    }

    @Override
    public int getSpacesCount() {
        return spaces.size();
    }

    @Override
    public double getTotalArea() {
        double totalArea = 0;
        for (Space space : spaces) {
            totalArea += space.getArea();
        }
        return totalArea;
    }

    @Override
    public int getTotalRoomsCount() {
        int totalRoomsCount = 0;
        for (Space space : spaces) {
            totalRoomsCount += space.getRoomsCount();
        }
        return totalRoomsCount;
    }

    @Override
    public Space[] getSpaces() {
        return spaces.toArray(new Space[spaces.size()]);
    }

    @Override
    public Space getSpace(int spaceNumber) {
        checkSpaceNumber(spaceNumber);
        return spaces.get(spaceNumber);
    }

    @Override
    public void setSpace(int spaceNumber, Space newSpace) {
        checkSpaceNumber(spaceNumber);
        spaces.set(spaceNumber, newSpace);
    }

    @Override
    public void addSpace(int spaceNumber, Space newSpace) {
        if (spaceNumber < 0 || spaceNumber > spaces.size()) {
            throw new SpaceIndexOutOfBoundsException();
        }
        spaces.add(spaceNumber, newSpace);
    }

    @Override
    public void removeSpace(int spaceNumber) {
        checkSpaceNumber(spaceNumber);
        spaces.remove(spaceNumber);
    }

    @Override
    public Space getBestSpace() {
        return Collections.max(spaces, (s1, s2) -> Double.compare(s1.getArea(), s2.getArea()));
    }

    @Override
    public Iterator<Space> iterator() {
        return spaces.iterator();
    }

    protected void checkSpaceNumber(int spaceNumber) {
        if (spaceNumber < 0 || spaceNumber >= spaces.size()) {
            throw new SpaceIndexOutOfBoundsException();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractFloor)) return false;

        AbstractFloor that = (AbstractFloor) o;

        return spaces.equals(that.spaces);

    }

    @Override
    public int hashCode() {
        return spaces.hashCode();
    }

    @Override
    public AbstractFloor clone() throws CloneNotSupportedException {
        AbstractFloor clone = (AbstractFloor) super.clone();
        clone.spaces = new ArrayList<>(spaces.size());
        for (Space space : spaces) {
            clone.spaces.add(space.clone());
        }
        return clone;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder(getClass().getSimpleName());
        info.append(" (").append(getSpacesCount());
        for (Space space : spaces) {
            info.append(", ").append(space.toString());
        }
        return info.append(")").toString();
    }
}
