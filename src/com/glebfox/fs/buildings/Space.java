package com.glebfox.fs.buildings;

import java.io.Serializable;

/**
 * @author glebfox
 */
public interface Space extends Serializable, Cloneable {

    double getArea();

    void setArea(double area);

    int getRoomsCount();

    void setRoomsCount(int roomsCount);

    Space clone() throws CloneNotSupportedException;
}
