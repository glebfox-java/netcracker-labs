package com.glebfox.fs.buildings;

import com.glebfox.fs.buildings.dwelling.DwellingFactory;
import com.glebfox.fs.buildings.dwelling.hotel.HotelFactory;
import com.glebfox.fs.buildings.office.OfficeFactory;

import javax.annotation.Nullable;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Formatter;
import java.util.Locale;
import java.util.Scanner;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public class Buildings {
    private static BuildingFactory factory = new DwellingFactory();

    public static void setFactory(BuildingFactory factory) {
        Buildings.factory = factory;
    }

    public static void setFactory(String buildingType) {
        switch (buildingType.toUpperCase()) {
            case "DWELLING":
                Buildings.setFactory(new DwellingFactory());
                break;
            case "OFFICE_BUILDING":
                Buildings.setFactory(new OfficeFactory());
                break;
            case "HOTEL":
                Buildings.setFactory(new HotelFactory());
                break;
            default:
                throw new IllegalArgumentException("Factory for building type: '" + buildingType + "' not found");
        }
    }

    public static Space createSpace(double area) {
        return factory.createSpace(area);
    }

    public static Space createSpace(double area, int roomsCount) {
        return factory.createSpace(area, roomsCount);
    }

    public static <T extends Space> T createSpace(Class<T> spaceClass) {
        Space space;
        try {
            Constructor<? extends Space> constructor = spaceClass.getDeclaredConstructor();
            space = constructor.newInstance();
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return spaceClass.cast(space);
    }

    public static <T extends Space> T createSpace(Class<T> spaceClass, double area) {
        Space space;
        try {
            Constructor<? extends Space> constructor = spaceClass.getDeclaredConstructor(double.class);
            space = constructor.newInstance(area);
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return spaceClass.cast(space);
    }

    public static <T extends Space> T createSpace(Class<T> spaceClass, double area, int roomsCount) {
        Space space;
        try {
            Constructor<? extends Space> constructor = spaceClass.getDeclaredConstructor(double.class, int.class);
            space = constructor.newInstance(area, roomsCount);
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return spaceClass.cast(space);
    }

    public static Floor createFloor(int spacesCount) {
        return factory.createFloor(spacesCount);
    }

    public static Floor createFloor(Space... spaces) {
        return factory.createFloor(spaces);
    }

    public static <T extends Floor> T createFloor(Class<T> floorClass, int spacesCount) {
        Floor floor;
        try {
            Constructor<? extends Floor> constructor = floorClass.getConstructor(int.class);
            floor = constructor.newInstance(spacesCount);
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return floorClass.cast(floor);
    }

    public static <T extends Floor> T createFloor(Class<T> floorClass, Space... spaces) {
        Floor floor;
        try {
            Constructor<? extends Floor> constructor = floorClass.getConstructor(Space[].class);
            floor = constructor.newInstance(new Object[]{spaces});
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return floorClass.cast(floor);
    }

    public static Building createBuilding(int... spacesCounts) {
        return factory.createBuilding(spacesCounts);
    }

    public static Building createBuilding(Floor... floors) {
        return factory.createBuilding(floors);
    }

    public static <T extends Building> T createBuilding(Class<T> buildingClass, int... spacesCounts) {
        Building building;
        try {
            Constructor<? extends Building> constructor = buildingClass.getConstructor(int[].class);
            building = constructor.newInstance(new Object[]{spacesCounts});
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return buildingClass.cast(building);
    }

    public static <T extends Building> T createBuilding(Class<T> buildingClass, Floor... floors) {
        Building building;
        try {
            Constructor<? extends Building> constructor = buildingClass.getConstructor(Floor[].class);
            building = constructor.newInstance(new Object[]{floors});
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException(e.getLocalizedMessage());
        }
        return buildingClass.cast(building);
    }

    public static void outputBuilding(Building building, OutputStream out) throws IOException {
        ObjectOutputStream outputStream = new ObjectOutputStream(out);
        outputStream.writeObject(building);
    }

    @Nullable
    public static Building inputBuilding(InputStream in) throws IOException, ClassNotFoundException {
        ObjectInputStream inputStream = new ObjectInputStream(in);
        Object building = inputStream.readObject();

        if (building != null) {
            return (Building) building;
        }
        return null;
    }

    public static void writeBuilding(Building building, Writer out) throws IOException {
        Formatter formatter = new Formatter(new Locale("English"));
//        Formatter formatter = new Formatter();
        Floor[] floors = building.getFloors();
        formatter.format("%d ", floors.length);
        for (Floor floor : building) {
            formatter.format("%d ", floor.getSpacesCount());
            for (Space space : floor) {
                formatter.format("%d %.1f ", space.getRoomsCount(), space.getArea());
            }
        }

        out.write(formatter.format("%n").toString());
    }

    @Nullable
    public static Building readBuilding(Reader in) throws IOException {
        StreamTokenizer tokenizer = new StreamTokenizer(in);
        tokenizer.parseNumbers();
        tokenizer.nextToken();
        Floor[] floors = new Floor[(int) tokenizer.nval];
        if (floors.length == 0)
            return null;        // может кидать ошибку?
        for (int floorIndex = 0; floorIndex < floors.length; floorIndex++) {
            tokenizer.nextToken();
            Space[] spaces = new Space[(int) tokenizer.nval];
            for (int spaceIndex = 0; spaceIndex < spaces.length; spaceIndex++) {
                tokenizer.nextToken();
                int roomsCount = (int) tokenizer.nval;
                tokenizer.nextToken();
                double area = tokenizer.nval;
                spaces[spaceIndex] = createSpace(area, roomsCount);
            }
            floors[floorIndex] = createFloor(spaces);
        }
        return createBuilding(floors);
    }

    @Nullable
    public static <T extends Building> T readBuilding(Reader in,
                                                      Class<T> buildingClass,
                                                      Class<? extends Floor> floorClass,
                                                      Class<? extends Space> spaceClass) throws IOException {
        StreamTokenizer tokenizer = new StreamTokenizer(in);
        tokenizer.nextToken();
        Floor[] floors = new Floor[(int) tokenizer.nval];
        if (floors.length == 0)
            return null;        // TODO: gg, может кидать ошибку?
        for (int floorIndex = 0; floorIndex < floors.length; floorIndex++) {
            tokenizer.nextToken();
            Space[] spaces = new Space[(int) tokenizer.nval];
            for (int spaceIndex = 0; spaceIndex < spaces.length; spaceIndex++) {
                tokenizer.nextToken();
                int roomsCount = (int) tokenizer.nval;
                tokenizer.nextToken();
                double area = tokenizer.nval;
                spaces[spaceIndex] = createSpace(spaceClass, area, roomsCount);
            }
            floors[floorIndex] = createFloor(floorClass, spaces);
        }
        return createBuilding(buildingClass, floors);
    }

    @Nullable
    public static Building readBuilding(Scanner in) {
        Floor[] floors = new Floor[in.nextInt()];
        if (floors.length == 0)
            return null;        // TODO: gg, может кидать ошибку?
        for (int floorIndex = 0; floorIndex < floors.length; floorIndex++) {
            Space[] spaces = new Space[in.nextInt()];
            for (int spaceIndex = 0; spaceIndex < spaces.length; spaceIndex++) {
                int roomsCount = in.nextInt();
                double area = in.nextDouble();
                spaces[spaceIndex] = createSpace(area, roomsCount);
            }
            floors[floorIndex] = createFloor(spaces);
        }
        return createBuilding(floors);
    }

    @Nullable
    public static <T extends Building> T readBuilding(Scanner in,
                                                      Class<T> buildingClass,
                                                      Class<? extends Floor> floorClass,
                                                      Class<? extends Space> spaceClass) {
        Floor[] floors = new Floor[in.nextInt()];
        if (floors.length == 0)
            return null;        // TODO: gg, может кидать ошибку?
        for (int floorIndex = 0; floorIndex < floors.length; floorIndex++) {
            Space[] spaces = new Space[in.nextInt()];
            for (int spaceIndex = 0; spaceIndex < spaces.length; spaceIndex++) {
                int roomsCount = in.nextInt();
                double area = in.nextDouble();
                spaces[spaceIndex] = createSpace(spaceClass, area, roomsCount);
            }
            floors[floorIndex] = createFloor(floorClass, spaces);
        }
        return createBuilding(buildingClass, floors);
    }

//    public static Floor synchronizedFloor(Floor floor) {
//        return new SynchronizedFloor(floor);
//    }
}
