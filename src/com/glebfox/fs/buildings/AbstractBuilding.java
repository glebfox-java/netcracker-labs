package com.glebfox.fs.buildings;

import com.glebfox.fs.exceptions.FloorIndexOutOfBoundsException;
import com.glebfox.fs.exceptions.SpaceIndexOutOfBoundsException;

import javax.annotation.Nullable;
import java.util.*;

/**
 * @author glebfox
 */
public abstract class AbstractBuilding implements Building {

    private static final long serialVersionUID = -635071252946251904L;

    protected List<Floor> floors;

    protected AbstractBuilding(int size) {
        floors = new ArrayList<>(size);
    }

    protected AbstractBuilding(Floor... floors) {
        this.floors = new ArrayList<>(floors.length);
        Collections.addAll(this.floors, floors);
    }

    @Override
    public int getFloorsCount() {
        return floors.size();
    }

    @Override
    public int getSpacesCount() {
        int totalSpacesCount = 0;
        for (Floor floor : floors) {
            totalSpacesCount += floor.getSpacesCount();
        }
        return totalSpacesCount;
    }

    @Override
    public double getTotalArea() {
        double totalArea = 0;
        for (Floor floor : floors) {
            totalArea += floor.getTotalArea();
        }
        return totalArea;
    }

    @Override
    public int getTotalRoomsCount() {
        int totalRoomsCount = 0;
        for (Floor floor : floors) {
            totalRoomsCount += floor.getTotalRoomsCount();
        }
        return totalRoomsCount;
    }

    @Override
    public Floor[] getFloors() {
        return floors.toArray(new Floor[floors.size()]);
    }

    @Override
    public Floor getFloor(int floorNumber) {
        checkFloorNumber(floorNumber);
        return floors.get(floorNumber);
    }

    @Override
    public void setFloor(int floorNumber, Floor newFloor) {
        checkFloorNumber(floorNumber);
        floors.set(floorNumber, newFloor);
    }

    @Nullable
    @Override
    public Space getSpace(int spaceNumber) {
        checkSpaceNumber(spaceNumber, false);

        for (Floor floor : floors) {
            int spacesCount = floor.getSpacesCount();
            if (spaceNumber < spacesCount) {
                return floor.getSpace(spaceNumber);
            } else {
                spaceNumber -= spacesCount;
            }
        }
        return null;
    }

    @Override
    public void setSpace(int spaceNumber, Space newSpace) {
        checkSpaceNumber(spaceNumber, false);

        for (Floor floor : floors) {
            int spacesCount = floor.getSpacesCount();
            if (spaceNumber < spacesCount) {
                floor.setSpace(spaceNumber, newSpace);
                return;
            } else {
                spaceNumber -= spacesCount;
            }
        }
    }

    @Override
    public void addSpace(int spaceNumber, Space newSpace) {
        checkSpaceNumber(spaceNumber, true);

        for (Floor floor : floors) {
            int flatsCount = floor.getSpacesCount();
            if (spaceNumber <= flatsCount) {
                floor.addSpace(spaceNumber, newSpace);
                return;
            } else {
                spaceNumber -= flatsCount;
            }
        }
    }

    @Override
    public void removeSpace(int spaceNumber) {
        checkSpaceNumber(spaceNumber, true);

        for (Floor floor : floors) {
            int spacesCount = floor.getSpacesCount();
            if (spaceNumber < spacesCount) {
                floor.removeSpace(spaceNumber);
            } else
                spaceNumber -= spacesCount;
        }
    }

    @Override
    public Space getBestSpace() {
        Space bestSpace = floors.get(0).getBestSpace();

        for (Floor floor : floors) {
            Space currFloorBestSpace = floor.getBestSpace();
            if (currFloorBestSpace.getArea() > bestSpace.getArea()) {
                bestSpace = currFloorBestSpace;
            }
        }
        return bestSpace;
    }

    @Override
    public Space[] getSortedSpaces() {
        List<Space> spaces = getAllSpaces();
        Collections.sort(spaces, Collections.reverseOrder((o1, o2) -> Double.compare(o1.getArea(), o2.getArea())));
        return spaces.toArray(new Space[spaces.size()]);
    }

    @Override
    public Iterator<Floor> iterator() {
        return floors.iterator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractBuilding)) return false;

        AbstractBuilding that = (AbstractBuilding) o;

        return floors.equals(that.floors);

    }

    @Override
    public int hashCode() {
        return floors.hashCode();
    }

    @Override
    public AbstractBuilding clone() throws CloneNotSupportedException {
        AbstractBuilding clone = (AbstractBuilding) super.clone();
        clone.floors = new ArrayList<>(floors.size());
        for (Floor floor : floors) {
            clone.floors.add(floor.clone());
        }
        return clone;
    }

    @Override
    public String toString() {
        StringBuilder info = new StringBuilder(getClass().getSimpleName());
        info.append(" (").append(getFloorsCount());
        for (Floor floor : floors) {
            info.append(", ").append(floor.toString());
        }

        return info.append(")").toString();
    }

    protected List<Space> getAllSpaces() {
        List<Space> spaces = new ArrayList<>(getSpacesCount());
        for (Floor floor : floors) {
            spaces.addAll(Arrays.asList(floor.getSpaces()));
        }
        return spaces;
    }

    protected void checkFloorNumber(int floorNumber) {
        if (floorNumber < 0 || floorNumber >= floors.size()) {
            throw new FloorIndexOutOfBoundsException();
        }
    }

    protected void checkSpaceNumber(int spaceNumber, boolean strong) {
        if (spaceNumber < 0 || strong
                ? spaceNumber > getSpacesCount()
                : spaceNumber >= getSpacesCount()) {
            throw new SpaceIndexOutOfBoundsException();
        }
    }
}
