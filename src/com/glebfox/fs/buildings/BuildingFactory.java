package com.glebfox.fs.buildings;

/**
 * @author glebfox
 */
public interface BuildingFactory {

    Space createSpace(double area);

    Space createSpace(double area, int roomsCount);

    Floor createFloor(int spacesCount);

    Floor createFloor(Space... spaces);

    Building createBuilding(int... spacesCounts);

    Building createBuilding(Floor... floors);
}
