package com.glebfox.fs.buildings;

import com.glebfox.fs.buildings.dwelling.Dwelling;
import com.glebfox.fs.buildings.dwelling.DwellingFloor;
import com.glebfox.fs.buildings.dwelling.Flat;
import com.glebfox.fs.buildings.dwelling.hotel.Hotel;
import com.glebfox.fs.buildings.dwelling.hotel.HotelFloor;
import com.glebfox.fs.buildings.office.Office;
import com.glebfox.fs.buildings.office.OfficeBuilding;
import com.glebfox.fs.buildings.office.OfficeFloor;
import org.apache.commons.lang3.RandomUtils;

import java.lang.reflect.Array;

/**
 * @author glebfox
 */
@SuppressWarnings("unused")
public final class BuildingTools {

    private BuildingTools() {
        throw new UnsupportedOperationException();
    }

    public static String getInfo(Space space) {
        return String.format("(%.2f %d)", space.getArea(), space.getRoomsCount());
    }

    public static String getInfo(Space... spaces) {
        StringBuilder info = new StringBuilder("[");
        for (int i = 0; i < spaces.length; i++) {
            info.append(i).append(getInfo(spaces[i])).append(" ");
        }
        return info.append("]").toString();
    }

    public static String getInfo(Floor floor) {
        return getInfo(floor.getSpaces());
    }

    public static String getInfo(Floor... floors) {
        StringBuilder info = new StringBuilder("{" + System.lineSeparator());
        for (int i = 0; i < floors.length; i++) {
            info.append(i).append(getInfo(floors[i].getSpaces())).append(System.lineSeparator());
        }
        return info.append("}").toString();
    }

    public static String getInfo(Building building) {
        return getInfo(building.getFloors());
    }

    public static void printInfo(Space space) {
        System.out.println(getInfo(space));
    }

    public static void printInfo(Space... spaces) {
        System.out.println(getInfo(spaces));
    }

    public static void printInfo(Floor floor) {
        printInfo(floor.getSpaces());
    }

    public static void printInfo(Floor... floors) {
        System.out.println(getInfo(floors));
    }

    public static void printInfo(Building building) {
        printInfo(building.getFloors());
    }

    public static double getRandomArea() {
        return RandomUtils.nextDouble(10.0, 100.0);
    }

    public static int getRandomRoomsCount() {
        return RandomUtils.nextInt(1, 4);
    }

    public static int getRandomSpacesCount() {
        return RandomUtils.nextInt(1, 10);
    }

    public static int getRandomFloorsCount() {
        return RandomUtils.nextInt(1, 9);
    }

    public static  int getRandomStarsCount() {
        return RandomUtils.nextInt(1, 5);
    }

    public static <T extends Space> T getRandomSpace(Class<T> spaceType) {
        return Buildings.createSpace(spaceType, getRandomArea(), getRandomRoomsCount());
    }

    public static <T extends Space> T[] getRandomSpaces(Class<T> spaceType) {
        // noinspection unchecked
        T[] spaces = (T[]) Array.newInstance(spaceType, getRandomSpacesCount());
        for (int i = 0; i < spaces.length; i++) {
            spaces[i] = getRandomSpace(spaceType);
        }
        return spaces;
    }

    public static <T extends Floor> T getRandomFloor(Class<T> floorType, Class<? extends Space> spaceType) {
        return Buildings.createFloor(floorType, (Space[]) getRandomSpaces(spaceType));
    }

    public static <T extends Floor> T[] getRandomFloors(Class<T> floorType, Class<? extends Space> spaceType) {
        // noinspection unchecked
        T[] floors = (T[]) Array.newInstance(floorType, getRandomFloorsCount());
        for (int i = 0; i < floors.length; i++) {
            floors[i] = getRandomFloor(floorType, spaceType);
        }
        return floors;
    }

    public static <T extends Building> T getRandomBuilding(Class<T> buildingType,
                                                           Class<? extends Floor> floorType, Class<? extends Space> spaceType) {
        return Buildings.createBuilding(buildingType, (Floor[]) getRandomFloors(floorType, spaceType));
    }

    public static Flat getRandomFlat() {
        return getRandomSpace(Flat.class);
    }

    public static Flat[] getRandomFlats() {
        return getRandomSpaces(Flat.class);
    }

    public static DwellingFloor getRandomDwellingFloor() {
        return getRandomFloor(DwellingFloor.class, Flat.class);
    }

    public static DwellingFloor[] getRandomDwellingFloors() {
        return getRandomFloors(DwellingFloor.class, Flat.class);
    }

    public static Dwelling getRandomDwelling() {
        return getRandomBuilding(Dwelling.class, DwellingFloor.class, Flat.class);
    }

    public static Office getRandomOffice() {
        return getRandomSpace(Office.class);
    }

    public static Office[] getRandomOffices() {
        return getRandomSpaces(Office.class);
    }

    public static OfficeFloor getRandomOfficeFloor() {
        return getRandomFloor(OfficeFloor.class, Office.class);
    }

    public static OfficeFloor[] getRandomOfficeFloors() {
        return getRandomFloors(OfficeFloor.class, Office.class);
    }

    public static OfficeBuilding getRandomOfficeBuilding() {
        return getRandomBuilding(OfficeBuilding.class, OfficeFloor.class, Office.class);
    }

    public static HotelFloor getRandomHotelFloor() {
        return getRandomFloor(HotelFloor.class, Flat.class);
    }

    public static HotelFloor[] getRandomHotelFloors() {
        return getRandomFloors(HotelFloor.class, Flat.class);
    }

    public static Hotel getRandomHotel() {
        Hotel hotel = getRandomBuilding(Hotel.class, HotelFloor.class, Flat.class);
        for (Floor floor : hotel) {
            if (floor instanceof HotelFloor) {
                ((HotelFloor) floor).setStars(getRandomStarsCount());
            }
        }
        return hotel;
    }
}
