package com.glebfox.fs.examples.building.lab2;

import com.glebfox.fs.buildings.Building;
import com.glebfox.fs.buildings.BuildingTools;
import com.glebfox.fs.buildings.dwelling.Dwelling;
import com.glebfox.fs.buildings.office.OfficeFloor;

import java.util.Random;

/**
 * @author glebfox
 */
public class Task3 {
    private static final Random random = new Random();

    public static void main(String[] args) {
        int[] flatsCount = new int[BuildingTools.getRandomFloorsCount()];
        for (int i = 0; i < flatsCount.length; i++) {
            flatsCount[i] = BuildingTools.getRandomSpacesCount();
        }
        Building building = new Dwelling(flatsCount);
        BuildingTools.printInfo(building);

        building = new Dwelling(BuildingTools.getRandomDwellingFloors());
        BuildingTools.printInfo(building.getFloors());

        System.out.println("total floors = " + building.getFloorsCount());
        System.out.println("total flats = " + building.getSpacesCount());
        System.out.println("total spaces = " + building.getTotalArea());
        System.out.println("total rooms = " + building.getTotalRoomsCount());

        int floorNumber = random.nextInt(building.getFloorsCount());
        System.out.println("floor at " + floorNumber);
        BuildingTools.printInfo(building.getFloor(floorNumber));

        System.out.println("set floor at " + floorNumber);
        building.setFloor(floorNumber, BuildingTools.getRandomDwellingFloor());
        BuildingTools.printInfo(building);

        int flatNumber = random.nextInt(building.getSpacesCount());
        System.out.println("flat at " + flatNumber);
        BuildingTools.printInfo(building.getSpace(flatNumber));

        System.out.println("set flat at " + flatNumber);
        building.setSpace(flatNumber, BuildingTools.getRandomFlat());
        BuildingTools.printInfo(building);

        flatNumber = random.nextInt(building.getSpacesCount());
        System.out.println("add flat at " + flatNumber);
        building.addSpace(flatNumber, BuildingTools.getRandomFlat());
        BuildingTools.printInfo(building);

        System.out.println("remove flat at " + flatNumber);
        building.removeSpace(flatNumber);
        BuildingTools.printInfo(building);

        System.out.println("best space = " + BuildingTools.getInfo(building.getBestSpace()));

        System.out.println("sorted flats:");
        BuildingTools.printInfo(building.getSortedSpaces());

        System.out.println("=====");
        building.setFloor(0, BuildingTools.getRandomOfficeFloor());
        building.setFloor(1, new OfficeFloor(10));
//        building.setFloor(0, new HotelFloor(10));
        BuildingTools.printInfo(building);
    }
}
