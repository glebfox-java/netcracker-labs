package com.glebfox.fs.examples.building.lab2;

import com.glebfox.fs.buildings.BuildingTools;
import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.dwelling.Flat;

/**
 * @author glebfox
 */
public class Task1 {
    public static void main(String[] args) {
        Space flat = new Flat();
        System.out.println(flat);

        flat = new Flat(BuildingTools.getRandomArea());
        System.out.println(flat);

        flat = new Flat(BuildingTools.getRandomArea(), BuildingTools.getRandomRoomsCount());
        System.out.println(flat);

        flat.setArea(BuildingTools.getRandomArea());
        flat.setRoomsCount(BuildingTools.getRandomRoomsCount());
        System.out.println(flat);
    }
}
