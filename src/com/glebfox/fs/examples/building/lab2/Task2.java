package com.glebfox.fs.examples.building.lab2;

import com.glebfox.fs.buildings.BuildingTools;
import com.glebfox.fs.buildings.Floor;
import com.glebfox.fs.buildings.dwelling.DwellingFloor;
import com.glebfox.fs.buildings.office.Office;

import java.util.Random;

/**
 * @author glebfox
 */
public class Task2 {
    private static final Random random = new Random();

    public static void main(String[] args) throws CloneNotSupportedException {
        Floor floor = new DwellingFloor(BuildingTools.getRandomSpacesCount());
        System.out.println(floor);

        floor = new DwellingFloor(BuildingTools.getRandomFlats());
        System.out.println(floor);

        System.out.println("flats count = " + floor.getSpacesCount());
        System.out.println("total space = " + floor.getTotalArea());
        System.out.println("total rooms = " + floor.getTotalRoomsCount());

        int flatNumber = random.nextInt(floor.getSpacesCount());
        System.out.println("random flat = " + flatNumber);
        System.out.println(floor.getSpace(flatNumber));

        System.out.println("set flat at " + flatNumber);
        floor.setSpace(flatNumber, BuildingTools.getRandomFlat());
        System.out.println(floor);

        System.out.println("new flat at " + flatNumber);
        floor.addSpace(flatNumber, BuildingTools.getRandomFlat());
        System.out.println(floor);

        System.out.println("new flat at " + floor.getSpacesCount());
        floor.addSpace(floor.getSpacesCount(), BuildingTools.getRandomFlat());
        System.out.println(floor);

        System.out.println("remove flat at " + flatNumber);
        floor.removeSpace(flatNumber);
        System.out.println(floor);

        System.out.println("remove flat at " + (floor.getSpacesCount() - 1));
        floor.removeSpace(floor.getSpacesCount() - 1);
        System.out.println(floor);

        System.out.println("best space = " + floor.getBestSpace());

        System.out.println("=====");
        floor.setSpace(0, BuildingTools.getRandomOffice());
        floor.setSpace(1, new Office());
        System.out.println(floor);
    }
}
