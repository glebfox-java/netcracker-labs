package com.glebfox.fs.buildings;

import org.junit.Assert;
import org.junit.Before;

/**
 * @author glebfox
 */
public abstract class FloorTest extends Assert {

    protected Floor floor1;
    protected Floor floor2;
    protected Floor floor3;

    @Before
    public void setUp() {

    }

    protected abstract Class<? extends Floor> getType();


}
