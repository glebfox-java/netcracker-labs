package com.glebfox.fs.buildings;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author glebfox
 */
public abstract class SpaceTest extends Assert {
    public static final double TEST_AREA_VALUE = 10.6;
    public static final int TEST_ROOMS_COUNT_VALUE = 2;
    
    protected Space space1;
    protected Space space2;
    protected Space space3;

    @Before
    public void setUp() {
        space1 = Buildings.createSpace(getType(), TEST_AREA_VALUE, TEST_ROOMS_COUNT_VALUE);
        space2 = Buildings.createSpace(getType(), TEST_AREA_VALUE, TEST_ROOMS_COUNT_VALUE);
        space3 = Buildings.createSpace(getType(), BuildingTools.getRandomArea(), BuildingTools.getRandomRoomsCount());
    }

    protected abstract Class<? extends Space> getType();

    @Test
    public void testDefaultConstructor() {
        Space space = Buildings.createSpace(getType());
        assertEquals(AbstractSpace.DEFAULT_ROOMS_COUNT, space.getRoomsCount(), 0);
        assertEquals(AbstractSpace.DEFAULT_AREA, space.getArea(), 0.0);
    }

    @Test
    public void testAreaConstructor() {
        Space space = Buildings.createSpace(getType(), TEST_AREA_VALUE);
        assertEquals(AbstractSpace.DEFAULT_ROOMS_COUNT, space.getRoomsCount(), 0);
        assertEquals(TEST_AREA_VALUE, space.getArea(), 0.0);
    }

    @Test
    public void testConstructor() {
        assertEquals(TEST_ROOMS_COUNT_VALUE, space1.getRoomsCount(), 0);
        assertEquals(TEST_AREA_VALUE, space1.getArea(), 0.0);
    }

    @Test
    public void testClone() throws CloneNotSupportedException {
        Space clone = space1.clone();
        assertEquals(space1, clone);
        assertNotSame(space1, clone);
        clone.setRoomsCount(10);
        assertNotEquals(space1, clone);
    }

    @Test
    public void testEqualsReflexivity() {
        assertEquals(space1, space1);
        assertEquals(space2, space2);
        assertEquals(space3, space3);
    }

    @Test
    public void testEqualsSymmetry() {
        assertTrue(space1.equals(space2) == space2.equals(space1));
    }

    @Test
    public void testEqualsTransitivity() {
        Space defaultSpace1 = Buildings.createSpace(getType());
        Space defaultSpace2 = Buildings.createSpace(getType());
        Space defaultSpace3 = Buildings.createSpace(getType());

        assertEquals(defaultSpace1, defaultSpace2);
        assertEquals(defaultSpace2, defaultSpace3);
        assertEquals(defaultSpace3, defaultSpace1);
    }

    @Test
    public void testEqualsConsistency() {
        assertTrue(space1.equals(space2) == space2.equals(space1));
    }

    @Test
    public void testEqualsNull() {
        assertNotEquals(space1, null);
        assertNotEquals(space2, null);
        assertNotEquals(space3, null);
    }

    @Test
    public void testHashCode() {
        assertEquals(space1.hashCode(), space2.hashCode());
    }

    @Test
    public void testAreaAccess() {
        Space space = Buildings.createSpace(getType());
        double area = BuildingTools.getRandomArea();
        space.setArea(area);
        assertEquals(area, space.getArea(), 0.0);
    }

    @Test
    public void testRoomsCountAccess() {
        Space space = Buildings.createSpace(getType());
        int roomsCount = BuildingTools.getRandomRoomsCount();
        space.setRoomsCount(roomsCount);
        assertEquals(roomsCount, space.getRoomsCount(), 0);
    }
}
