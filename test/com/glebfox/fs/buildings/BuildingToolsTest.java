package com.glebfox.fs.buildings;

import com.glebfox.fs.buildings.dwelling.DwellingFloor;
import com.glebfox.fs.buildings.dwelling.Flat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author glebfox
 */
public class BuildingToolsTest extends Assert {
    private static final String SPACE_INFO = "(54.30 2)";
    private static final String FLOOR_INFO = "[0(54.30 2) 1(67.50 1) 2(50.00 2) ]";
    private static final String BUILDING_INFO = "";

    private Space space;
    private Floor floor;
    private Building building;

    @Before
    public void setUpData() {
        space = new Flat(54.3, 2);
        Space[] spaces = new Space[]{new Flat(54.3, 2), new Flat(67.5, 1), new Flat()};
        floor = new DwellingFloor(spaces);
    }

    @Test
    public void testSpaceInfo() {
        assertTrue(SPACE_INFO.equals(BuildingTools.getInfo(space)));
    }

    @Test
    public void testFloorInfo() {
        assertTrue(FLOOR_INFO.equals(BuildingTools.getInfo(floor)));
    }

    @Test
    public void testBuildingInfo() {

    }
}
