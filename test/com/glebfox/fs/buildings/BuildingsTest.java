package com.glebfox.fs.buildings;

import com.glebfox.fs.buildings.dwelling.Dwelling;
import com.glebfox.fs.buildings.dwelling.DwellingFactory;
import com.glebfox.fs.buildings.dwelling.DwellingFloor;
import com.glebfox.fs.buildings.dwelling.Flat;
import com.glebfox.fs.buildings.dwelling.hotel.Hotel;
import com.glebfox.fs.buildings.dwelling.hotel.HotelFactory;
import com.glebfox.fs.buildings.dwelling.hotel.HotelFloor;
import com.glebfox.fs.buildings.office.Office;
import com.glebfox.fs.buildings.office.OfficeBuilding;
import com.glebfox.fs.buildings.office.OfficeFactory;
import com.glebfox.fs.buildings.office.OfficeFloor;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author glebfox
 */
public class BuildingsTest extends Assert {
    public static final double TEST_AREA = 10.6;
    public static final int TEST_ROOMS_COUNT = 2;
    public static final int TEST_SPACES_COUNT = 10;
    static final int[] TEST_SPACES_COUNT_ARRAY = {2, 4, 3, 5};

    @Test
    public void testFlatCreation() {
        Buildings.setFactory(new DwellingFactory());

        // test area constructor
        Flat flat1 = new Flat(TEST_AREA);
        Flat flat2 = (Flat) Buildings.createSpace(TEST_AREA);
        assertEquals(flat1, flat2);

        flat2 = Buildings.createSpace(Flat.class, TEST_AREA);
        assertEquals(flat1, flat2);

        // test full constructor
        flat1 = new Flat(TEST_AREA, TEST_ROOMS_COUNT);
        flat2 = (Flat) Buildings.createSpace(TEST_AREA, TEST_ROOMS_COUNT);
        assertEquals(flat1, flat2);

        flat2 = Buildings.createSpace(Flat.class, TEST_AREA, TEST_ROOMS_COUNT);
        assertEquals(flat1, flat2);

        // test default constructor
        flat1 = new Flat();
        flat2 = Buildings.createSpace(Flat.class);
        assertEquals(flat1, flat2);
    }

    @Test
    public void testOfficeCreation() {
        Buildings.setFactory(new OfficeFactory());

        // test area constructor
        Office office1 = new Office(TEST_AREA);
        Office office2 = (Office) Buildings.createSpace(TEST_AREA);
        assertEquals(office1, office2);

        office2 = Buildings.createSpace(Office.class, TEST_AREA);
        assertEquals(office1, office2);

        // test full constructor
        office1 = new Office(TEST_AREA, TEST_ROOMS_COUNT);
        office2 = (Office) Buildings.createSpace(TEST_AREA, TEST_ROOMS_COUNT);
        assertEquals(office1, office2);

        office2 = Buildings.createSpace(Office.class, TEST_AREA, TEST_ROOMS_COUNT);
        assertEquals(office1, office2);

        // test default constructor
        office1 = new Office();
        office2 = Buildings.createSpace(Office.class);
        assertEquals(office1, office2);
    }

    @Test
    public void testDwellingFloorCreation() {
        Buildings.setFactory(new DwellingFactory());

        // test space count constructor
        DwellingFloor floor1 = new DwellingFloor(TEST_SPACES_COUNT);
        DwellingFloor floor2 = (DwellingFloor) Buildings.createFloor(TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(DwellingFloor.class, TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        // test spaces constructor
        Space[] spaces = BuildingTools.getRandomSpaces(Flat.class);
        floor1 = new DwellingFloor(spaces);
        floor2 = (DwellingFloor) Buildings.createFloor(spaces);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(DwellingFloor.class, spaces);
        assertEquals(floor1, floor2);
    }

    @Test
    public void testOfficeFloorCreation() {
        Buildings.setFactory(new OfficeFactory());

        // test spaces count constructor
        OfficeFloor floor1 = new OfficeFloor(TEST_SPACES_COUNT);
        OfficeFloor floor2 = (OfficeFloor) Buildings.createFloor(TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(OfficeFloor.class, TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        // test spaces constructor
        Space[] spaces = BuildingTools.getRandomSpaces(Office.class);
        floor1 = new OfficeFloor(spaces);
        floor2 = (OfficeFloor) Buildings.createFloor(spaces);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(OfficeFloor.class, spaces);
        assertEquals(floor1, floor2);
    }

    @Test
    public void testHotelFloorCreation() {
        Buildings.setFactory(new HotelFactory());

        // test spaces count constructor
        HotelFloor floor1 = new HotelFloor(TEST_SPACES_COUNT);
        HotelFloor floor2 = (HotelFloor) Buildings.createFloor(TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(HotelFloor.class, TEST_SPACES_COUNT);
        assertEquals(floor1, floor2);

        // test spaces constructor
        Space[] spaces = BuildingTools.getRandomSpaces(Flat.class);
        floor1 = new HotelFloor(spaces);
        floor2 = (HotelFloor) Buildings.createFloor(spaces);
        assertEquals(floor1, floor2);

        floor2 = Buildings.createFloor(HotelFloor.class, spaces);
        assertEquals(floor1, floor2);
    }

    @Test
    public void testDwellingCreation() {
        Buildings.setFactory(new DwellingFactory());

        // test spaces count constructor
        Dwelling dwelling1 = new Dwelling(TEST_SPACES_COUNT_ARRAY);
        Dwelling dwelling2 = (Dwelling) Buildings.createBuilding(TEST_SPACES_COUNT_ARRAY);
        assertEquals(dwelling1, dwelling2);

        dwelling2 = Buildings.createBuilding(Dwelling.class, TEST_SPACES_COUNT_ARRAY);
        assertEquals(dwelling1, dwelling2);

        // test floors constructor
        Floor[] floors = BuildingTools.getRandomFloors(DwellingFloor.class, Flat.class);
        dwelling1 = new Dwelling(floors);
        dwelling2 = (Dwelling) Buildings.createBuilding(floors);
        assertEquals(dwelling1, dwelling2);

        dwelling2 = Buildings.createBuilding(Dwelling.class, floors);
        assertEquals(dwelling1, dwelling2);
    }

    @Test
    public void testOfficeBuildingCreation() {
        Buildings.setFactory(new OfficeFactory());

        // test spaces count constructor
        OfficeBuilding officeBuilding1 = new OfficeBuilding(TEST_SPACES_COUNT_ARRAY);
        OfficeBuilding officeBuilding2 = (OfficeBuilding) Buildings.createBuilding(TEST_SPACES_COUNT_ARRAY);
        assertEquals(officeBuilding1, officeBuilding2);

        officeBuilding2 = Buildings.createBuilding(OfficeBuilding.class, TEST_SPACES_COUNT_ARRAY);
        assertEquals(officeBuilding1, officeBuilding2);

        // test floors constructor
        Floor[] floors = BuildingTools.getRandomFloors(OfficeFloor.class, Office.class);
        officeBuilding1 = new OfficeBuilding(floors);
        officeBuilding2 = (OfficeBuilding) Buildings.createBuilding(floors);
        assertEquals(officeBuilding1, officeBuilding2);

        officeBuilding2 = Buildings.createBuilding(OfficeBuilding.class, floors);
        assertEquals(officeBuilding1, officeBuilding2);
    }

    @Test
    public void testHotelCreation() {
        Buildings.setFactory(new HotelFactory());

        // test spaces count constructor
        Hotel hotel1 = new Hotel(TEST_SPACES_COUNT_ARRAY);
        Hotel hotel2 = (Hotel) Buildings.createBuilding(TEST_SPACES_COUNT_ARRAY);
        assertEquals(hotel1, hotel2);

        hotel2 = Buildings.createBuilding(Hotel.class, TEST_SPACES_COUNT_ARRAY);
        assertEquals(hotel1, hotel2);

        // test floors constructor
        Floor[] floors = BuildingTools.getRandomFloors(HotelFloor.class, Flat.class);
        hotel1 = new Hotel(floors);
        hotel2 = (Hotel) Buildings.createBuilding(floors);
        assertEquals(hotel1, hotel2);

        hotel2 = Buildings.createBuilding(Hotel.class, floors);
        assertEquals(hotel1, hotel2);
    }
}
