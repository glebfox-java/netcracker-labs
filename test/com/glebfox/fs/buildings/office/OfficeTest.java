package com.glebfox.fs.buildings.office;

import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.SpaceTest;

/**
 * @author glebfox
 */
public class OfficeTest extends SpaceTest {
    @Override
    protected Class<? extends Space> getType() {
        return Office.class;
    }
}
