package com.glebfox.fs.buildings.dwelling;

import com.glebfox.fs.buildings.Space;
import com.glebfox.fs.buildings.SpaceTest;

/**
 * @author glebfox
 */
public class FlatTest extends SpaceTest {
    @Override
    protected Class<? extends Space> getType() {
        return Flat.class;
    }
}
