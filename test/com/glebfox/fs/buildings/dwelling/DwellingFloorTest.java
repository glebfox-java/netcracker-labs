package com.glebfox.fs.buildings.dwelling;

import com.glebfox.fs.buildings.AbstractFloor;
import com.glebfox.fs.buildings.AbstractSpace;
import com.glebfox.fs.buildings.BuildingTools;
import com.glebfox.fs.buildings.Space;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author glebfox
 */
public class DwellingFloorTest extends Assert {
    private static final AbstractSpace DEFAULT_FLAT = new Flat();
    private DwellingFloor floor1;
    
    @Before
    public void setUpData() {
        floor1 = new DwellingFloor(3);
    }
    
    @Test
    public void testSpaceChanging() {
        AbstractFloor floor = new DwellingFloor(3);
        for (Space space : floor) {
            assertEquals(DEFAULT_FLAT, space);
        }
        
        Space space = floor.getSpace(1);
        space.setRoomsCount(5);
        
        
    }
    
    @Test
    public void testClone() throws CloneNotSupportedException {
        DwellingFloor clone = floor1.clone();
        assertEquals(floor1, clone);
        assertNotSame(floor1, clone);

        clone.setSpace(1, new Flat(10.0, 1));
        assertNotEquals(floor1, clone);

        clone = floor1.clone();
        for (int i = 0; i < floor1.getSpacesCount(); i++) {
            Space space = clone.getSpace(i);
            assertNotSame(floor1.getSpace(i), space);
            space.setArea(BuildingTools.getRandomArea());
            space.setRoomsCount(BuildingTools.getRandomRoomsCount());
        }
        assertNotEquals(floor1, clone);
    }
}
